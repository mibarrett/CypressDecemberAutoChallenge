/// <reference types="cypress" />

context('Checkboxes', () => {
  beforeEach(() => {
    cy.visit('http://the-internet.herokuapp.com/checkboxes')
  })

  // check un-checked checkbox - assert it becomes checked
  it('.check() - click on a DOM element', () => {
    //cy.get('checkbox').within('$checkboxes').check().should('be.checked')
    cy.get('#checkboxes > input[type=checkbox]:nth-child(1)').check().should('be.checked')

    // click new element to delete it - assert it no longer exists
    cy.get('#checkboxes > input[type=checkbox]:nth-child(3)').uncheck().should('not.to.be.checked')
  })
})