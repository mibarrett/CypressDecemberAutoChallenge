/// <reference types="cypress" />

context('Actions', () => {
  beforeEach(() => {
    cy.visit('http://the-internet.herokuapp.com/dynamic_controls')
  })

  // check the checkbox
  it('.check() - check a checkbox or radio element', () => {
    cy.get('[type="checkbox"]').check({
      timeout: 3000
    }).should('be.checked')
  })

  // click button to remove check
  it('.click() - click on a DOM element', () => {
    cy.get('#checkbox-example > button').click({
      timeout: 3000
    })
    cy.get('[type="checkbox"]').should('not.to.exist')
  })

  //click the enable button to un-grey input box
  it('.click() - click on a DOM element', () => {
    cy.get('#input-example > button').click({
      timeout: 3000
    })
    cy.get('#input-example > input[type=text]').should('to.be.enabled')
    cy.get('#input-example > input[type=text]')
      .type('text entered while box active').should('have.value', 'text entered while box active')
    cy.get('#input-example > button').click({
      timeout: 3000
    })
  })
})