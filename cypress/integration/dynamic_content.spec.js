/// <reference types="cypress" />

context('Dynamic Content', () => {
  beforeEach(() => {
    cy.visit('/dynamic_content')
  })

  it("Assert there are 3 pictures", () => {
    cy.get("div#content.large-10.columns").find('img')
      .should(($el) => {
        expect($el).to.have.length(3)
      })
  })

  it("Assert there are 3 paragraphs", () => {
    cy.get("div#content.large-10.columns").find('.large-10.columns')
      .should(($el) => {
        expect($el).to.have.length(3)
      })
  })
})