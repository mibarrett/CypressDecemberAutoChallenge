/// <reference types="cypress" />

context('Status Codes', () => {
    beforeEach(() => {
        cy.visit('/status_codes')
    })

    //Find 404 and click on it url should contain 404
    it('click 404', () => {
        cy.get('#content > div > ul > li:nth-child(3) > a').click()
        cy.url().should('contain', '404')
    })
})