/// <reference types="cypress" />

context('Context Menu', () => {
  beforeEach(() => {
    cy.visit('/context_menu')
  })

  // trigger DOM element
  it('.trigger context menu', () => {
    cy.get('#hot-spot').trigger('contextmenu')
    cy.get('#content > script').should('exist')
  })
})