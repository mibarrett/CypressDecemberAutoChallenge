/// <reference types="cypress" />

context('Inputs', () => {
    beforeEach(() => {
        cy.visit('/inputs')
    })

    //Focus the input box - Click up arrow three times and assert box displays 3
    it('.focus() - focuses on a DOM element', () => {
        cy.get('#content > div > div > div > input[type=number]').focus({
                timeout: 10000
            })
            .type('{uparrow},{uparrow},{uparrow},')
        cy.get('input').should('have.value', '3')
    })
})