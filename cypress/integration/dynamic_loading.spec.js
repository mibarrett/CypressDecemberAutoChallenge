/// <reference types="cypress" />

context('Dynamic Loading', () => {
    beforeEach(() => {
        cy.visit('/dynamic_loading')
    })
    it("Dynamic Loading", () => {
        cy.get('#content > div > a:nth-child(5)').click()
        // click start button
        cy.get('#start > button').invoke('show').click()
        cy.wait(10000)
        cy.get('#finish > h4').then($el => {
            cy.wrap($el).invoke('show')
                .should('be.visible')
        })
    })
})