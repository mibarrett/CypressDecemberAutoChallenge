require('cypress-downloadfile/lib/downloadFileCommand')
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

// cypress image compare
/* <reference types="cypress-image-compare" />

import "cypress-image-compare/commands"; */

// snapshot compare command
/*const compareSnapshotCommand = require('cypress-visual-regression/dist/plugin');
compareSnapshotCommand(); */

// snapshot command https://medium.com/norwich-node-user-group/visual-regression-testing-with-cypress-io-and-cypress-image-snapshot-99c520ccc595
/*import { addMatchImageSnapshotCommand } from 'cypress-image-snapshot/command';
addMatchImageSnapshotCommand({
  failureThreshold: 0.00,
  failureThresholdType: 'percent',
  customDiffConfig: { threshold: 0.0 },
  capture: 'viewport',
});
Cypress.Commands.add("setResolution", (size) => {
  if (Cypress._.isArray(size)) {
     cy.viewport(size[0], size[1]);
   } else {
    cy.viewport(size);
  }
 })*/

// drag and drop command
Cypress.Commands.add("dragTo", {
  prevSubject: "element"
}, (subject, targetEl) => {
  cy.wrap(subject).trigger("dragstart");
  cy.get(targetEl).trigger("drop");
});

Cypress.Commands.add("triggerHover", {
  prevSubject: true
}, function (elements) {
  cy.log('crash');
  return elements;
});

import { addMatchImageSnapshotCommand } from 'cypress-image-snapshot/command';

addMatchImageSnapshotCommand();







//import 'cypress-wait-until';
// image compare commands
// import { addMatchImageSnapshotCommand } from 'cypress-image-snapshot/command';

//addMatchImageSnapshotCommand();

// image diff commands
/*import { addMatchImageSnapshotCommand } from 'cypress-image-diff/command';

addMatchImageSnapshotCommand(options);

addMatchImageSnapshotCommand({
  failureThreshold: 0.03, // threshold for entire image
  failureThresholdType: 'percent', // percent of image or number of pixels
  customDiffConfig: { threshold: 0.1 }, // threshold for each pixel
  capture: 'viewport', // capture viewport in screenshot
});*/