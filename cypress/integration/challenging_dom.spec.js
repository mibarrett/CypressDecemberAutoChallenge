/// <reference types="cypress" />



require;
'pixelmatch'
require;
'matchImageSnapshot'


context('Challenge DOM', () => {
  beforeEach(() => {
    cy.visit('/challenging_dom')
  })
  // take screenshot - click button - take another screenshot
  it('takes screenshot of canvas', () => {

    let imgA = cy.get('#canvas').matchImageSnapshot({timeout: 3000})
    //let imgA = cy.get('#canvas').screenshot()
    cy.get('body').then(($body) => {
      if ($body.text().includes('foo')) {
        cy.get('.button').contains('foo').click({
          force: true
        }, {
          timeout: 3000
        })
      } else if ($body.text().includes('baz')) {
        cy.get('.button').contains('baz').click({
          force: true
        }, {
          timeout: 3000
        }) //.trigger('mousedown',{force: true})
      } else {
        cy.get('.button').contains('qux').click({
          force: true
        }, {
          timeout: 3000
        }) //.trigger('mousedown',{force: true})
      }

    })
    let imgB = cy.get('#canvas').matchImageSnapshot({timeout: 3000})
    //let imgB = cy.get('#canvas').screenshot()

    // the comparison image can be found in ..\cypress\snapshots\challenging_dom.spec.js\__diff_output__
    it('compares the snapshots', () => {
      cy.matchImageSnapshot(imgA, imgB, null, 599, 200, {
        threshold: 0.1,
        diffContext: putImageData(diff, 0, 0),
      })
    })
  })
  // assert title of column 1 contains 'Lorem'
  it('.get() - get a DOM element', () => {
    cy.get('#content > div > div > div > div.large-10.columns > table > thead > tr > th:nth-child(1)')
      .should('contain', 'Lorem')
  })
})