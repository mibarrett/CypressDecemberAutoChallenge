/// <reference types="cypress" />

context('Typos', () => {
    beforeEach(() => {
        cy.visit('/typos')
    })
// Assert that paragraph contains correct spelling - test fails if spelling is incorrect
    it("Checks for the typo", () => {
      cy.get('#content > div > p:nth-child(3)').contains("won't") 
    })
})