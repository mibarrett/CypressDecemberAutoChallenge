/// <reference types="cypress" />

context('Add Elements', () => {
  beforeEach(() => {
    cy.visit('/add_remove_elements/')
  })

  // click add element button - assert new element exists
  it('.click() - click on a DOM element', () => {
    cy.get('#content > div > button').click({
      timeout: 10000
    })
    cy.get('#elements > button').should('exist')

    // click new element to delete it - assert it no longer exists
    cy.get('#elements> button').click({
      timeout: 10000
    })
    cy.get('#elements > button').should('not.exist')
  })
})