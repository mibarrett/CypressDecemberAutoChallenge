/// <reference types="cypress" />

context('Infinite Scroll', () => {
    beforeEach(() => {

    })
// sometimes this needs to be refreshed - I didn't figure out how to get it to waitUntil '.jscroll-added
    it("Scrolls Down", () => {
        let numberOfChildren = 1
        cy.visit('/infinite_scroll')
        for (let i = 0; i < 5; i++) {
            cy.get('#content > div > div > div > div > div')
                //.children()
                .then($children => {
                    cy.wrap($children)
                        .its('length')
                        .should('eq', numberOfChildren + 1)
                })
            cy.scrollTo('bottom', {
                    duration: 3000
                })
                .get('.jscroll-added', {timeout: 3000}).should('be.visible')
                .then(() => numberOfChildren = numberOfChildren + 1)
        }
    })
})
