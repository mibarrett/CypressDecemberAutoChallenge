/// <reference types="cypress" />

context('Hovers', () => {
  beforeEach(() => {
    cy.visit('/hovers')
  })

  /*describe('hovers', function () {
    ['mouseover'].forEach((event) => {
      it(`triggers event: '${event}`, function () {
        cy.get(':nth-child(4) > img').invoke('trigger', event)
        cy.get('#content > div > div:nth-child(4) > div').should('contain', 'name: user2')
      })
    })
  })*/

  // hover over image - my initial solution
  it('.hover() - hover over 2nd image', () => {
    cy.get(':nth-child(4) > img').trigger('mouseover')
  
    // assert caption is visible
    cy.get('#content > div > div:nth-child(4) > div').then($el => {
      cy.wrap($el).invoke('show')
        .should('contain', 'name: user2') 

    })
  })
})
  
  
