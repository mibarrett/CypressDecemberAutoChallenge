/// <reference types="cypress" />

context('Login', () => {
    beforeEach(() => {
        cy.visit('https://admin:admin@the-internet.herokuapp.com/basic_auth')
    })

    it('validates login success', () => {
        cy.get('#content > div > p').should('include.text', 'Congratulations')
    })
})