/// <reference types="cypress" />

context('Horizontal Slider', () => {
  beforeEach(() => {
    cy.visit('/horizontal_slider')
  })

  it('.invoke() - get the range slider', () => {
    cy.get('input[type=range]').as('range')
      .invoke('val', 3)
      .trigger('change')

    // assert slider value is 3
    cy.get('input').should('have.value', '3')

  })
})