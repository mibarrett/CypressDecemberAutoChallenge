/// <reference types="cypress" />

context('Broken Images', () => {
  beforeEach(() => {
    cy.visit('/broken_images')
  })

  it("should display a image in element div with class image", () => {
    cy.get('#content > div > img:nth-child(2)').find("img").should('be.visible');
  })
})