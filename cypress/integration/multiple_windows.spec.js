/// <reference types="cypress" />

context('Multiple windows', () => {
    beforeEach(() => {
        cy.visit('/windows')
    })
    // opens the link but prevent it from opening new window - then asserts url contains 'new'
    it('Clicks the link for new window', function () {
        cy.visit('/windows')
        cy.get('#content > div > a').invoke('removeAttr', 'target').click()
        cy.url().should('include', 'http://the-internet.herokuapp.com/windows/new')
    })
    /*it('Clicks the link for new window', function () {
        cy.visit('/windows')
        cy.window().then((win) => {
            cy.stub(win, 'open').as('windowOpen')
        })*/

        /*// asserts name of url in new window
        it('checks url of new window', function () {
            cy.visit('http://the-internet.herokuapp.com/windows/new')
            cy.url().should('include', 'http://the-internet.herokuapp.com/windows/new')

        })*/
       /* // test the target of the link
        cy.get('#content > div > a').click()
        cy.get('#content > div > a').should('have.attr', 'target', '_blank') 
    })*/
})