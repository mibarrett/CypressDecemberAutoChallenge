/// <reference types="cypress" />

import '@4tw/cypress-drag-drop'

context('Drag and Drop', () => {
  beforeEach(() => {
    cy.visit('/drag_and_drop')
  })

  it('should dragndrop', () => {
    cy.get('#column-a').drag('#column-b')
    cy.get('#column-a').should('include.text', 'B')
    cy.get('#column-b').should('include.text', 'A')
  })
})