/// <reference types="cypress" />
/// <reference types="cypress-downloadfile"/>

context('Download', () => {
    beforeEach(() => {
        cy.visit('/download')
    })
// downloads file and confirms that file has been downloaded
    it('dowloads the file', () => {
        cy.downloadFile('http://the-internet.herokuapp.com/download/luminoslogo.png', 'mydownloads', 'example.jpg')
    })
})