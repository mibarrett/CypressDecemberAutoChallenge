/// <reference types="cypress" />

context('Dropdown', () => {
    beforeEach(() => {
        cy.visit('/dropdown')
    })
    // Select option 2
    it('.select() - select 2nd option from dropdown', () => {
        cy.get('#dropdown').select('Option 2')
        // Assert Option 2 is selected
        cy.get('#dropdown').should('have.value', '2')
        // Assert Option 2 is displayed
        cy.get('option[selected="selected"]').should('have.text', 'Option 2')   
    })
})