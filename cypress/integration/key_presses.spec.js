/// <reference types="cypress" />

context('Key Presses', () => {
    beforeEach(() => {
        cy.visit('/key_presses')
    })
    // Press J key
    it('.get() then type J', () => {
        cy.get('#target').type('J')
       // Assert Result and input are same
        cy.get('#result').should('have.text', 'You entered: J')   
    })
})