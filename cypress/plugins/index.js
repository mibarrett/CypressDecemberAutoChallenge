/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */
const {downloadFile} = require('cypress-downloadfile/lib/addPlugin')
module.exports = (on, config) => {
    on('task', {downloadFile})
  }

  const {
    addMatchImageSnapshotPlugin,
  } = require('cypress-image-snapshot/plugin');
  
  module.exports = (on, config) => {
    addMatchImageSnapshotPlugin(on, config);
  };

  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config

/*const {downloadFile} = require('cypress-downloadfile/lib/addPlugin')
module.exports = (on, config) => {
on('task', {downloadFile})
} */
  /*const getCompareSnapshotsPlugin = require('cypress-visual-regression/dist/plugin');

  module.exports = (on) => {
    getCompareSnapshotsPlugin(on);
  }; */


  
  /*module.exports = (on, config) => {
 addMatchImageSnapshotPlugin(on, config);
  }*/

    // snapshot plugin https://medium.com/norwich-node-user-group/visual-regression-testing-with-cypress-io-and-cypress-image-snapshot-99c520ccc595
  /*const {
    addMatchImageSnapshotPlugin,
  } = require('cypress-image-snapshot/plugin');  */
  
  


  


  // image diff plugin
  /*const {
    addMatchImageSnapshotPlugin,
  } = require('cypress-image-snapshot/plugin');
  module.exports = on => {
    addMatchImageSnapshotPlugin(on, config);
  };*/
